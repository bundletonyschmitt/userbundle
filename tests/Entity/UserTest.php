<?php

namespace TonySchmitt\UserBundle\Tests\Entity;

use TonySchmitt\UserBundle\Entity\User;
use PHPUnit\Framework\TestCase;

class UserTest extends TestCase
{
  /**
   * @var User
   */
  protected $object;

  public $adminRole = "ROLE_ADMIN";

  protected function setUp()
  {
    $this->object = new User();
  }

  public function testGetterAndSetter()
  {
    $this->assertNull($this->object->getId());
    $this->assertNull($this->object->eraseCredentials());

    $this->assertTrue($this->object->isEnabled());

    $roles = array("ROLE_USER");
    $this->assertEquals($roles, $this->object->getRoles());

    $roles[] = $this->adminRole;
    $this->object->addRole($this->adminRole);
    $this->assertEquals($roles, $this->object->getRoles());

    $this->object->removeRole("ROLE_USER");
    $this->assertEquals(array($this->adminRole), $this->object->getRoles());

    $id = 1;
    $this->object->setId($id);
    $this->assertEquals($id, $this->object->getId());

    $email = "test@gmail.com";
    $this->object->setEmail($email);
    $this->assertEquals($email, $this->object->getEmail());

    $username = "UserNameTest";
    $this->object->setUsername($username);
    $this->assertEquals($username, $this->object->getUsername());

    $pass = "thisisp@ssword";
    $this->object->setPlainPassword($pass);
    $this->assertEquals($pass, $this->object->getPlainPassword());

    $password_hash = hash('sha512', $pass);
    $this->object->setPassword($password_hash);
    $this->assertEquals($password_hash, $this->object->getPassword());

    $this->assertNull($this->object->getSalt());

    $this->assertTrue($this->object->isAccountNonExpired());
    $this->assertTrue($this->object->isAccountNonLocked());
    $this->assertTrue($this->object->isCredentialsNonExpired());

    $s = serialize(array('1', 'TestUser', 'p@ssword', true));
    $this->object->unserialize($s);
    $this->assertEquals($s, $this->object->serialize());

  }
}
