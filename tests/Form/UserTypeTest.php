<?php

namespace TonySchmitt\UserBundle\Tests\Form;

use TonySchmitt\UserBundle\Form\UserType;
use TonySchmitt\UserBundle\Entity\User;
use Symfony\Component\Form\Test\TypeTestCase;

class UserTypeTest extends TypeTestCase
{
    public $pass = 'p@ssword';


    public function testSubmitValidData()
    {
        $formData = array(
            'email' => 'test@gmail.com',
            'username' => 'TestUser',
            'plainPassword' => array(
                'first' => $this->pass,
                'second' => $this->pass
            )
        );

        $form = $this->factory->create(UserType::class);

        $object = new User();
        $object->setUsername('TestUser');
        $object->setEmail('test@gmail.com');
        $object->setPlainPassword($this->pass);
        // ...populate $object properties with the data stored in $formData

        // submit the data to the form directly
        $form->submit($formData);

        $this->assertTrue($form->isSynchronized());
        $this->assertEquals($object, $form->getData());

        $view = $form->createView();
        $children = $view->children;

        foreach (array_keys($formData) as $key) {
            $this->assertArrayHasKey($key, $children);
        }
    }
}
