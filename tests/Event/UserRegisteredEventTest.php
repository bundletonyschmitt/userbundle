<?php

namespace TonySchmitt\UserBundle\Tests\Event;

use TonySchmitt\UserBundle\Event\UserRegisteredEvent;
use TonySchmitt\UserBundle\Entity\User;
use PHPUnit\Framework\TestCase;

class UserRegisteredEventTest extends TestCase
{
  /**
   * @var User
   */
  protected $object;

  protected function setUp()
  {
    $user = new User();
    $user->setId(1);
    $this->object = new UserRegisteredEvent($user);
  }

  public function testGetter()
  {
    $this->assertEquals(1, $this->object->getUserId());
  }
}
