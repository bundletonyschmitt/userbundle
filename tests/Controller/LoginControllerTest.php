<?php

namespace TonySchmitt\UserBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\BrowserKit\Cookie;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

class LoginControllerTest extends WebTestCase
{
    private $client = null;

    public function setUp()
    {
        $this->client = static::createClient();
    }

    public function testLoginPage()
    {
        $crawler = $this->client->request('GET', '/login');

        $this->assertGreaterThan(
            0,
            $crawler->filter('h3:contains("Login")')->count()
        );
    }

    public function testLoggedPage() {
      $this->logIn();

      $crawler = $this->client->request('GET', '/login');

      $this->assertGreaterThan(
          0,
          $crawler->filter('html:contains("Logged")')->count()
      );
    }

    private function logIn()
    {
        $session = $this->client->getContainer()->get('session');

        // the firewall context defaults to the firewall name
        $firewallContext = 'main';

        $token = new UsernamePasswordToken('batman', null, $firewallContext, array('ROLE_ADMIN'));
        $session->set('_security_'.$firewallContext, serialize($token));
        $session->save();

        $cookie = new Cookie($session->getName(), $session->getId());
        $this->client->getCookieJar()->set($cookie);
    }
}
