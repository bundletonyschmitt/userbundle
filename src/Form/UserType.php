<?php

namespace TonySchmitt\UserBundle\Form;

use TonySchmitt\UserBundle\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $classForm = array('class' => 'form-control form-control-lg rounded-0');
        $builder
            ->add('email', EmailType::class, array('attr'=> $classForm, 'required' => true))
            ->add('username', TextType::class,array('attr'=> $classForm, 'required' => true))
            ->add('plainPassword', RepeatedType::class, array(
                'type' => PasswordType::class,
                'first_options'  => array('label' => 'Password', 'attr'=> $classForm),
                'second_options' => array('label' => 'Repeat Password', 'attr'=> $classForm),
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => User::class,
        ));
    }
}
