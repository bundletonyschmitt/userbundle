<?php

namespace TonySchmitt\UserBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use TonySchmitt\UserBundle\Entity\User;
use Doctrine\ORM\EntityManager;

class CreateUserCommand extends Command
{
    private $password_encoder;
    private $em;

    public function __construct(\Doctrine\ORM\EntityManager $em, \Symfony\Component\DependencyInjection\Container $container) {
        $this->password_encoder = $container->get('security.password_encoder');
        $this->em = $em;

        parent::__construct();
    }

    protected function configure()
    {
        $this
            // the name of the command (the part after "bin/console")
            ->setName('tonyschmitt:create-user')

            // the short description shown while running "php bin/console list"
            ->setDescription('Creates a new user.')

            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp('This command allows you to create a user...')

            //Arguments
            ->addArgument('username', InputArgument::REQUIRED, 'The username of the user.')
            ->addArgument('mail', InputArgument::REQUIRED, 'The mail of the user.')
            ->addArgument('password', InputArgument::REQUIRED, 'The password of the user.')
            ->addArgument('role', InputArgument::REQUIRED, 'The role of the user.')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // outputs multiple lines to the console (adding "\n" at the end of each line)
        $output->writeln([
            'User Creator',
            '============',
            '',
        ]);

        // 1) build the form
        $user = new User();

        // 3) Encode the password (you could also do this via Doctrine listener)
        $password = $this->password_encoder->encodePassword($user, $input->getArgument('password'));
        $user->setPassword($password);
        $user->addRole($input->getArgument('role'));
        $user->setUsername($input->getArgument('username'));
        $user->setEmail($input->getArgument('mail'));

        // 4) save the User!
        $em = $this->em;
        $em->persist($user);
        $em->flush();
        
        $output->writeln('User created');
    }
}