<?php

namespace TonySchmitt\UserBundle\Event\Listener;

use Symfony\Component\Console\Event\ConsoleCommandEvent;
use Symfony\Component\Console\Event\ConsoleTerminateEvent;
use TonySchmitt\ConfigBundle\Entity\BundleVersion;
use TonySchmitt\ConfigBundle\Event\Listener\UpdateSchemaListener;

class UpdateSchemaUserListener extends UpdateSchemaListener {

    protected $schema_version = 0;
    protected $bundle_name = 'tonyschmitt_user';

    function actionBefore() {
        // Ajouter ici le code permettant de mettre à jour la table
        // Ce Code permet de mettre à jour les données de la table existante vers la nouvelle version
    }

    function actionAfter()  {
        // Ajouter ici le code permettant de mettre à jour la table après doctrine:schema:update
        parent::actionAfter();
    }
}