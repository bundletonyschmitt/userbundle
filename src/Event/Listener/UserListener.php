<?php

namespace TonySchmitt\UserBundle\Event\Listener;

use TonySchmitt\UserBundle\Event\UserRegisteredEvent;
use TonySchmitt\CronBundle\Entity\Cron;

class UserListener {

    private $mailer;

    private $templating;

    private $container;

    private $mailService;

    function __construct(\Swift_Mailer $mailer, $templating, \Symfony\Component\DependencyInjection\Container $container, \TonySchmitt\MailBundle\Service\MailService $mailService) {
        $this->mailer = $mailer;
        $this->templating = $templating;
        $this->container = $container;
        $this->mailService = $mailService;
    }

    function onRegistered(UserRegisteredEvent $event) {
        $user = $event->getUser();
        $name = $user->getUsername();
        $mail = $user->getEmail();

        $bodyHtml = $this->templating->render(
            '@TonySchmittUser/Emails/registration.html.twig',
            array('name' => $name, 'mail' => $mail)
        );

        $parameters = array(
            'object' => 'Nouveau Compte',
            'mailTo' => $mail,
            'bodyHtml' => $bodyHtml
        );

        $cron = new Cron();

        $cron->setName('Nouveau Compte créé : ' . $name);
        $cron->setService('TonySchmitt\\MailBundle\\Service\\MailService');
        $cron->setParameters($parameters);
        $cron->setFunction('sendMail');

        $em = $this->getDoctrine()->getManager();
        $em->persist($cron);
        $em->flush();
    }
}