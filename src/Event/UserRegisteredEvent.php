<?php

namespace TonySchmitt\UserBundle\Event;

use Symfony\Component\EventDispatcher\Event;
use TonySchmitt\UserBundle\Entity\User;

class UserRegisteredEvent extends Event {

  const NAME = 'user.registered';

  /**
   * @var User
   */
  private $user;

  public function __construct(User $user) {
    $this->user = $user;
  }

  public function getUserId() {
    return $this->user->getId();
  }

  public function getUser() {
    return $this->user;
  }

}
