<?php

namespace TonySchmitt\UserBundle\Event;

use Symfony\Component\EventDispatcher\Event;
use TonySchmitt\UserBundle\Entity\User;

class UserOnRegisterEvent extends Event {

  const NAME = 'user.onRegister';

  /**
   * @var User
   */
  private $user;

  /**
   * @var array
   */
  private $errors;

  /**
   * @var array
   */
  private $options;

  public function __construct(User $user, $options = array()) {
    $this->user = $user;
    $this->errors = array();
    $this->options = $options;
  }

  public function getUser() {
    return $this->user;
  }

  public function hasError() {
    if(count($this->errors) > 0) {
      return true;
    }
    return false;
  }

  public function addError($error) {
    $this->errors[] = $error;

    return $this;
  }

  public function getOptions() {
    return $this->options;
  }

  public function addOption($option, $optionKey = null) {
    if($optionKey != null) {
      $this->options[$optionKey] = $option;
    } else {
      $this->options[] = $option;
    }

    return $this;
  }

  public function getErrors() {
    return $this->errors;
  }

}
