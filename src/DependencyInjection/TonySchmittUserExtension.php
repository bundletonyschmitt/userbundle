<?php

namespace TonySchmitt\UserBundle\DependencyInjection;

use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\Extension\PrependExtensionInterface;

class TonySchmittUserExtension extends extension implements PrependExtensionInterface
{
  public function load(array $configs, ContainerBuilder $container)
  {
    $loader = new YamlFileLoader(
      $container,
      new FileLocator(__DIR__.'/../Resources/config')
    );
  }

  public function prepend(ContainerBuilder $container)
    {
        $loader = new YamlFileLoader(
          $container,
          new FileLocator(__DIR__.'/../Resources/config')
        );
        $loader->load('services.yaml');
        $loader->load('easy_admin.yaml');
        $loader->load('framework.yaml');

        $this->addAnnotatedClassesToCompile(array(
          'TonySchmitt\\UserBundle\\Entity\\User',
        ));
    }
}
