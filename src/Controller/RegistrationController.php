<?php

namespace TonySchmitt\UserBundle\Controller;

use TonySchmitt\UserBundle\Form\UserType;
use TonySchmitt\UserBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use TonySchmitt\UserBundle\Event\UserRegisteredEvent;
use TonySchmitt\UserBundle\Event\UserOnRegisterEvent;

class RegistrationController extends Controller
{
    /**
     * @var EventDispatcherInterface
     */
    private $dispatcher;


    public function __construct(EventDispatcherInterface $dispatcher) {
        $this->dispatcher = $dispatcher;
    }

    public function registerAction(Request $request)
    {
        // 1) build the form
        $user = new User();
        $form = $this->createForm(UserType::class, $user);

        // 2) handle the submit (will only happen on POST)
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

          // On crée l'évènement avec ses 2 arguments
          $event = new UserOnRegisterEvent($user);

          // On déclenche l'évènement
          $this->get('event_dispatcher')->dispatch(UserOnRegisterEvent::NAME, $event);

          if(!$event->hasError()) {
              // On récupère ce qui a été modifié par le ou les listeners, ici le message
              $user->setOptions($event->getOptions());

              // 3) Encode the password (you could also do this via Doctrine listener)
              $password = $this->get('security.password_encoder')->encodePassword($user, $user->getPlainPassword());
              $user->setPassword($password);

              // 4) save the User!
              $em = $this->getDoctrine()->getManager();
              $em->persist($user);
              $em->flush();

              // ... do any other work - like sending them an email, etc
              // maybe set a "flash" success message for the user
              $this->dispatcher->dispatch(UserRegisteredEvent::NAME, new UserRegisteredEvent($user));

              return $this->redirectToRoute('login');
          }

        }

        return $this->render(
            '@TonySchmittUser/Security/register.html.twig',
            array('form' => $form->createView())
        );
    }
}
