<?php

namespace TonySchmitt\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class LoginController extends controller
{
  function loginAction(Request $request, AuthenticationUtils $authUtils) {

    $securityContext = $this->container->get('security.authorization_checker');

    if(!$securityContext->isGranted('IS_AUTHENTICATED_FULLY')) {
      // get the login error if there is one
      $error = $authUtils->getLastAuthenticationError();

      // last username entered by the user
      $lastUsername = $authUtils->getLastUsername();

      return $this->render(
        '@TonySchmittUser/Security/login.html.twig',
        array(
          'last_username' => $lastUsername,
          'error'         => $error,
        )
      );
    } else {
      return $this->render(
        '@TonySchmittUser/Security/logged.html.twig',
        array(
          'user' => $this->getUser()
        )
      );
    }

  }

}
