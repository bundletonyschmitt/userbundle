<?php

namespace TonySchmitt\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use TonySchmitt\UserBundle\Entity\User;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class PasswordController extends controller
{
    private $mailer;

    private $templating;

    function __construct(\Swift_Mailer $mailer, $templating) {
        $this->mailer = $mailer;
        $this->templating = $templating;
    }

    function passwordLostAction() {

        $error = null;
        $em = $this->getDoctrine();

        if(isset($_POST) && isset($_POST['_email'])) {
            $email = $_POST['_email'];
            if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
                $repository = $em->getRepository(User::class);

                if($user = $repository->findOneBy(array("email" => $email))) {
                    $randToken = rand(100000, 1000000);
                    $user->setPasswordToken($randToken);
                    $em->getManager()->persist($user);
                    $em->getManager()->flush();
                    $this->sendPasswordToken($user);
                    return $this->redirectToRoute('changePassword-token', array('userId' => $user->getId()));
                } else {
                    $error = 'Cet email n\'existe pas.';
                }
            } else {
                $error = 'Cet email a un format non adapté.';
            }
        }

        return $this->render(
            '@TonySchmittUser/Security/password-lost.html.twig',
            array('error' => $error)
        );

    }

    function sendPasswordToken($user) {
        $name = $user->getUsername();
        $mail = $user->getEmail();
        $passwordToken = $user->getPasswordToken();

        $message = (new \Swift_Message('Nouveau Mot de Passe'))
        ->setFrom([$this->getParameter('tonyschmitt_contactmail')['email'] => $this->getParameter('tonyschmitt_contactmail')['name']])
        ->setTo($mail)
        ->setBody(
            $this->templating->render(
                '@TonySchmittUser/Emails/password-lost.html.twig',
                array('name' => $name, 'mail' => $mail, 'token' => $passwordToken)
            ),
            'text/html'
        );

        $this->mailer->send($message);
    }

    function resetPasswordTokenAction($userId, UserPasswordEncoderInterface $passwordEncoder) {

        $error = null;
        $em = $this->getDoctrine();

        if(isset($_POST) && isset($_POST['_token']) && isset($_POST['_password']) && isset($_POST['_password-confirm'])) {
            $repository = $em->getRepository(User::class);

            if($user = $repository->findOneBy(array("id" => $userId))) {
                if($_POST['_token'] == $user->getPasswordToken() && $user->getPasswordToken() != NULL && $user->getPasswordToken() != '') {
                    if($_POST['_password'] == $_POST['_password-confirm']) {
                        $password = $this->get('security.password_encoder')->encodePassword($user, $_POST['_password']);
                        $user->setPassword($password);
                        $user->setPasswordToken(null);
                        $em->getManager()->persist($user);
                        $em->getManager()->flush();
                        return $this->redirectToRoute('login');
                    } else {
                        $error = 'Les mots de passe ne correspondent pas.';
                    }
                } else {
                    $error = 'Le code ne correspond pas.';
                }
            } else {
                $error = 'Erreur, veuillez retourner sur la page de connection <a href="/login">Login page</a>.';
            }
        }

        return $this->render(
            '@TonySchmittUser/Security/password-lost-token.html.twig',
            array('error' => $error, 'userId' => $userId)
        );

    }

}
