<?php

namespace TonySchmitt\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use TonySchmitt\UserBundle\Entity\User;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use TonySchmitt\CronBundle\Entity\Cron;

class UserProfilController extends controller
{
  public function __construct(\Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface $userPasswordEncoderInterface)  {
      $this->userPasswordEncoderInterface = $userPasswordEncoderInterface;
  }

  function profilAction(Request $request) {

    // creates a task and gives it some dummy data for this example
    $user = $this->getUser();

    $classForm = array('class' => 'form-control form-control-lg rounded-0');

    $errors = array();

    $form = $this->createFormBuilder($user)
        ->add('email', EmailType::class, array('attr'=> $classForm, 'required' => true))
        ->add('username', TextType::class,array('attr'=> $classForm, 'required' => true))
        ->add('save', SubmitType::class, array('label' => 'Edit User'))
        ->getForm();

        $form->handleRequest($request);

    if ($form->isSubmitted()) {
      $post = $_POST['form'];
      if($post['username'] == '') {
        $errors[] = 'The username cannot be empty';
      } else {
        $user->setUsername($post['username']);
      }

      if($post['email'] == '') {
        $errors[] = 'The email cannot be empty';
      } else {
        $user->setEmail($post['email']);
      }

      if($errors == null || count($errors) <= 0) {
        $em = $this->getDoctrine()->getManager();
        $em->persist($user);
        $em->flush();

        return $this->redirect('/');
      }
    }

    return $this->render(
      '@TonySchmittUser/Security/profil.html.twig',
      array(
        'form' => $form->createView(),
        'errors' => $errors
      )
    );

  }

  function passwordAction(Request $request) {

    // creates a task and gives it some dummy data for this example
    $user = $this->getUser();

    $classForm = array('class' => 'form-control form-control-lg rounded-0');

    $errors = array();

    $form = $this->createFormBuilder($user)
        ->add('plainPassword', RepeatedType::class, array(
            'type' => PasswordType::class,
            'first_options'  => array('label' => 'Password', 'attr'=> $classForm),
            'second_options' => array('label' => 'Repeat Password', 'attr'=> $classForm),
        ))
        ->add('save', SubmitType::class, array('label' => 'Edit User'))
        ->getForm();

        $form->handleRequest($request);

    if ($form->isSubmitted() && $form->isValid()) {
      $post = $_POST['form'];

      if($post['plainPassword'] == '') {
        $errors[] = 'The password cannot be empty';
      } else {
        $password = $this->userPasswordEncoderInterface->encodePassword($user, $user->getPlainPassword());
        $user->setPassword($password);
      }

      if(count($errors) <= 0) {
        $em = $this->getDoctrine()->getManager();
        $em->persist($user);
        $em->flush();
      }

      return $this->redirect('/');

    }

    return $this->render(
      '@TonySchmittUser/Security/password.html.twig',
      array(
        'form' => $form->createView(),
        'errors' => $errors
      )
    );

  }

  function createUsersAction(Request $request) {

    $classForm = array('class' => 'form-control form-control-lg rounded-0');

    $errors = array();

    $form = $this->createFormBuilder()
        ->add('csv', FileType::class, array('label' => 'Fichier CSV avec la liste d\'utilisateurs (1ère colonne : user, 2e colonne : mail)' ,'required' => true))
        ->add('save', SubmitType::class, array('label' => 'Ajouter des utilisateurs'))
        ->getForm();

    $form->handleRequest($request);

    if ($form->isSubmitted()) {
      $post = $_POST['form'];

      // Set path to CSV file
      $csvFile = $_FILES['form']['tmp_name']['csv'];
      
      $csv = $this->readCSV($csvFile);

      foreach ($csv as $user_csv) {
        if($user_csv) {
          $user_array = explode(';',$user_csv[0]);
          $name = $user_array[0];
          $mail = $user_array[1];

          $user = new User();
          $user->setUsername($name);
          $user->setEmail($mail);

          $password = $this->generateRandomString();
          $password_encode = $this->get('security.password_encoder')->encodePassword($user, $password);

          $user->setPassword($password_encode);

          $bodyHtml = $this->render(
            '@TonySchmittUser/Emails/new-account.html.twig',
            array('name' => $name, 'password' => $password, 'host' => $_SERVER["HTTP_ORIGIN"])
          )->getContent();
          
          $parameters = array(
            'object' => 'Nouveau Compte créé',
            'mailTo' => $mail,
            'bodyHtml' => $bodyHtml
          );
    
          $cron = new Cron();
    
          $cron->setName('Nouveau Compte créé : ' . $name);
          $cron->setService('TonySchmitt\\MailBundle\\Service\\MailService');
          $cron->setFunction('sendMail');
          $cron->setParameters($parameters);
    
          $em = $this->getDoctrine()->getManager();
          $em->persist($user);
          $em->persist($cron);
          $em->flush();
        }
      }

      return $this->redirect('/admin/');
    }

    return $this->render(
      '@TonySchmittUser/Security/create-users.html.twig',
      array(
        'form' => $form->createView(),
        'errors' => $errors
      )
    );

  }

  public function readCSV($csvFile){
    $file_handle = fopen($csvFile, 'r');
    while (!feof($file_handle) ) {
        $line_of_text[] = fgetcsv($file_handle, 1024);
    }
    fclose($file_handle);
    return $line_of_text;
  }

  public function generateRandomString($length = 10) {
    $characters = '123456789abcdefghjkmnpqrstuvwxyzABCDEFGHJKMNPQRSTUVWXYZ*&()_-;,:!?%';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
  }
 

}
