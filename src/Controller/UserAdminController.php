<?php

namespace TonySchmitt\UserBundle\Controller;

use EasyCorp\Bundle\EasyAdminBundle\Controller\AdminController as BaseAdminController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use TonySchmitt\CronBundle\Entity\Cron;

class UserAdminController extends BaseAdminController
{

  private $mailService;

  function __construct(\TonySchmitt\MailBundle\Service\MailService $mailService) {
      $this->mailService = $mailService;
  }

  public function indexAction(Request $request) {
    $this->denyAccessUnlessGranted('ROLE_SUPER_ADMIN', null, 'Vous n\'avez pas les droits nécessaires !');
    return parent::indexAction($request);
  }

  public function prePersistEntity($entity) {
    $password = $this->generateRandomString();
    $password_encode = $this->get('security.password_encoder')->encodePassword($this->getUser(), $password);
    $entity->setPassword($password_encode);

    $name = $entity->getUsername();
    $mail = $entity->getEmail();

    $bodyHtml = $this->render(
      '@TonySchmittUser/Emails/new-account.html.twig',
      array('name' => $name, 'password' => $password, 'host' => $_SERVER["HTTP_ORIGIN"])
  )->getContent();

    $parameters = array(
      'object' => 'Nouveau Compte créé',
      'mailTo' => $mail,
      'bodyHtml' => $bodyHtml
    );

    $cron = new Cron();

    $cron->setName('Nouveau Compte créé : ' . $name);
    $cron->setService('TonySchmitt\\MailBundle\\Service\\MailService');
    $cron->setParameters($parameters);
    $cron->setFunction('sendMail');

    $em = $this->getDoctrine()->getManager();
    $em->persist($cron);
    $em->flush();
  }

  public function generateRandomString($length = 10) {
    $characters = '123456789abcdefghjkmnpqrstuvwxyzABCDEFGHJKMNPQRSTUVWXYZ*&()_-;,:!?%';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
  }

}
