After installation replace security.yaml with the text :

"
security:
    # https://symfony.com/doc/current/book/security.html#where-do-users-come-from-user-providers
    encoders:
        TonySchmitt\UserBundle\Entity\User:
            algorithm: sha512

    # ...

    providers:
        our_db_provider:
            entity:
                class: TonySchmitt\UserBundle\Entity\User
                property: username

    firewalls:
        dev:
            pattern: ^/(_(profiler|wdt)|css|images|js)/
            security: false
        main:
            anonymous: ~
            provider: our_db_provider
            form_login:
                login_path: login
                check_path: login
                default_target_path: admin
            remember_me:
                secret:   '%kernel.secret%'
                lifetime: 604800 # 1 week in seconds
                path:     /
            logout:
                path:   /logout
                target: /

            # activate different ways to authenticate

            # http_basic: ~
            # https://symfony.com/doc/current/book/security.html#a-configuring-how-your-users-will-authenticate

            # form_login: ~
            # https://symfony.com/doc/current/cookbook/security/form_login_setup.html
    access_control:
        # require ROLE_ADMIN for /admin*
        - { path: ^/login, roles: IS_AUTHENTICATED_ANONYMOUSLY }
        - { path: ^/admin, roles: ROLE_ADMIN }

    role_hierarchy:
        ROLE_ADMIN:       ROLE_USER
        ROLE_SUPER_ADMIN: [ROLE_ADMIN, ROLE_ALLOWED_TO_SWITCH]
"

Pour créer un utilisateur utiliser la command ci-dessous
php bin/console tonyschmitt:create-user <username> <mail> <password> <role>
